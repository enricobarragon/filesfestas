
package FilesFestas.FXML;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

/**
 *
 * @author Enrico
 */
public class AddFesta extends Controller {
    @FXML
    private TextField nomeFesta;
    @FXML
    private TextField tipoFesta;
    @FXML
    private TextField custoIngresso;
    @FXML
    private TextField local;
    @FXML
    private Button voltar;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }


    @FXML
    public void getMenu(){
        this.stage.setScene(menu());
        this.stage.setFullScreen(false);
    }
    
    
     private Scene menu() {

        Scene scene = null;
        try {
            
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Menu.class.getResource("Menu.fxml"));
            AnchorPane rootLayout = (AnchorPane) loader.load();

            scene = new Scene(rootLayout);
              //observe essas duas linhas
            Menu m = loader.getController();
            m.setStage(this.stage);

        } catch (IOException e) {
            System.out.println("Deu erro!");
            e.printStackTrace();
        }
        return scene;
    }

  

  }
    


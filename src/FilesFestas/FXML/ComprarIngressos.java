/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FilesFestas.FXML;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

/**
 *
 * @author Enrico
 */
public class ComprarIngressos extends Controller {
    
    public void initialize(URL url, ResourceBundle rb) {
    }
    
    @FXML 
    private TextField email;
    @FXML
    private Button adquirir;
    @FXML
    private Button voltar;
    @FXML 
    private ListView selecFesta;
    
    @FXML
    public void getMenu(){
        this.stage.setScene(menu());
        this.stage.setFullScreen(false);
    }
    
    
     private Scene menu() {

        Scene scene = null;
        try {
            
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Menu.class.getResource("Menu.fxml"));
            AnchorPane rootLayout = (AnchorPane) loader.load();

            scene = new Scene(rootLayout);
              
            Menu m = loader.getController();
            m.setStage(this.stage);

        } catch (IOException e) {
            System.out.println("Deu erro");
            e.printStackTrace();
        }
        return scene;
    }
    
    
    
}

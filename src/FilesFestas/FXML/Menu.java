/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FilesFestas.FXML;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;

/**
 *
 * @author Enrico
 */
public class Menu extends Controller {

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
    }
    
    @FXML
    private Button novoCliente;
    @FXML
    private Button addFesta;
    @FXML
    private Button comprarIngresso;
    @FXML
    private Button verFestas;
    
    @FXML
    public void getNovoCliente(){
        this.stage.setScene(novoCliente());
        this.stage.setFullScreen(false);
    }
    
    
     private Scene novoCliente() {

        Scene scene = null;
        try {
            
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(NovoCliente.class.getResource("NovoCliente.fxml"));
            AnchorPane rootLayout = (AnchorPane) loader.load();

            scene = new Scene(rootLayout);
              
            NovoCliente m = loader.getController();
            m.setStage(this.stage);

        } catch (IOException e) {
            System.out.println("Deu erro");
            e.printStackTrace();
        }
        return scene;
    }
    
     @FXML
    public void getaddFesta(){
        this.stage.setScene(addFesta());
        this.stage.setFullScreen(false);
    }
    
    
     private Scene addFesta() {

        Scene scene = null;
        try {
            
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(AddFesta.class.getResource("AddFesta.fxml"));
            AnchorPane rootLayout = (AnchorPane) loader.load();

            scene = new Scene(rootLayout);
              //observe essas duas linhas
            AddFesta m = loader.getController();
            m.setStage(this.stage);

        } catch (IOException e) {
            System.out.println("Deu erro");
            e.printStackTrace();
        }
        return scene;
    }
    
     @FXML
    public void getComprarIngresso(){
        this.stage.setScene(comprarIngresso());
        this.stage.setFullScreen(false);
    }
    
    
     private Scene comprarIngresso() {

        Scene scene = null;
        try {
            
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(ComprarIngressos.class.getResource("ComprarIngressos.fxml"));
            AnchorPane rootLayout = (AnchorPane) loader.load();

            scene = new Scene(rootLayout);
              //observe essas duas linhas
            ComprarIngressos m = loader.getController();
            m.setStage(this.stage);

        } catch (IOException e) {
            System.out.println("Deu erro");
            e.printStackTrace();
        }
        return scene;
    }
     
     @FXML
    public void getVerFestas(){
        this.stage.setScene(verFestas());
        this.stage.setFullScreen(false);
    }
    
    
     private Scene verFestas() {

        Scene scene = null;
        try {
            
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(VerFestas.class.getResource("VerFestas.fxml"));
            AnchorPane rootLayout = (AnchorPane) loader.load();

            scene = new Scene(rootLayout);
              //observe essas duas linhas
            VerFestas m = loader.getController();
            m.setStage(this.stage);

        } catch (IOException e) {
            System.out.println("Deu erro");
            e.printStackTrace();
        }
        return scene;
    }
     
    }
    
    
    
    


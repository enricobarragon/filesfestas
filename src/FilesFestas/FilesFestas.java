/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FilesFestas;

import FilesFestas.FXML.Menu;
import java.io.IOException;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 *
 * @author Aluno
 */
public class FilesFestas extends Application {
    
    private Stage primaryStage; //isso não havia em outras classes
    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;

        primaryStage.setTitle("FilesFestas");
        primaryStage.setScene(getRootLayout());
        primaryStage.setFullScreen(false);
        primaryStage.show();
    }
    
     private Scene getRootLayout() {

        Scene scene = null;
        try {
            // Loader
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Menu.class.getResource("Menu.fxml"));
            AnchorPane rootLayout = (AnchorPane) loader.load();
            
            //observe essas duas linhas
            Menu tc = loader.getController();
            tc.setStage(primaryStage);
            
            scene = new Scene(rootLayout);

        } catch (IOException e) {
            System.out.println("Deu erro!");
            e.printStackTrace();
        }
        return scene;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}

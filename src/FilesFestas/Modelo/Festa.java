
package FilesFestas.Modelo;

/**
 *
 * @author Aluno
 */
public class Festa {
    private String nomeFesta;
    private String tipo;
    private double preco;
    private int nIngressos;
    private String Local;

    public String getLocal() {
        return Local;
    }

    public void setLocal(String Local) {
        this.Local = Local;
    }

    public String getNomeFesta() {
        return nomeFesta;
    }

    public void setNomeFesta(String nomeFesta) {
        this.nomeFesta = nomeFesta;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public int getnIngressos() {
        return nIngressos;
    }

    public void setnIngressos(int nIngressos) {
        this.nIngressos = nIngressos;
    }
    
    
}
